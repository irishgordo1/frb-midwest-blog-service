terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile                     = "default"
  region                      = "us-west-2"
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

resource "aws_iam_role" "poc_iam_lambda_role" {
  name = "iam_for_lambda"

  assume_role_policy = aws_iam_policy.poc_lambda_policy.arn
}

resource "aws_iam_policy" "poc_lambda_policy" {
  name = "policy-poc-lambda-000001"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["logs:CreateLogStream", "logs:PutLogEvents"]
        Resource = aws_lambda_function.frb_chi_blog_midwest_service.arn
        Effect   = "Allow"
      },
      {
        Action   = ["logs:CreateLogGroup"],
        Effect   = "Allow",
        Resource = "arn:aws:logs:us-west-2:153878479331:*"
      }
    ]
  })
}



resource "aws_lambda_function" "frb_chi_blog_midwest_service" {
  filename                       = "/builds/irishgordo1/frb-midwest-blog-service/function.zip"
  function_name                  = "frb-midwest-blog-service"
  role                           = aws_iam_role.poc_iam_lambda_role.arn
  handler                        = "main.lambda_handler"
  runtime                        = "python3.8"
  timeout                        = 60
  memory_size                    = 2048
  reserved_concurrent_executions = 1
  description                    = "proof of concept"
}
