import os
import logging
from src.services.BlogScrapeMidwest import BlogScrapeMidwest
import jsonpickle
from aws_xray_sdk.core import patch_all

logger = logging.getLogger()
logger.setLevel(logging.INFO)
patch_all()


def lambda_handler(event, context):
    logger.info('## ENVIRONMENT VARIABLES\r' +
                jsonpickle.encode(dict(**os.environ)))
    logger.info('## EVENT\r' + jsonpickle.encode(event))
    logger.info('## CONTEXT\r' + jsonpickle.encode(context))
    new_grab = BlogScrapeMidwest()
    new_grab.scrape()
    return True
