from abc import ABC, abstractmethod

"""
BlogScrape: abstract scraping class, interface
"""


class BlogScrape(ABC):
    @abstractmethod
    def scrape(self):
        """performs scraping"""
        pass
