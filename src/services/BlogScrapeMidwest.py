from src.repositories.BlogRepoMidwest import BlogRepoMidwest
from src.models.MidwestBlogModel import MidwestBlogModel
from typing import Tuple
from requests.models import Response
from src.services.BlogScrape import BlogScrape
from bs4 import BeautifulSoup
import requests
import threading
from time import perf_counter


class BlogScrapeMidwest(BlogScrape):
    def __init__(self) -> None:
        super().__init__()
        self.blog_base_url = 'https://www.chicagofed.org/publications/blogs/midwest-economy'
        self.total_pages = 1
        self.home_url = 'https://www.chicagofed.org'
        self.threads = []
        pass

    def _initialize(self, bs4_parser: BeautifulSoup = None,
                    page_obj: Response = None) -> Tuple[(bool), (Exception)]:
        if page_obj.status_code != 200:
            err = Exception(
                "Midwest Economy Blog page did not initialize correctly")
            return (False, err)
        else:
            max_pages = bs4_parser.find_all(
                "input", attrs={"class": "cfedResults--paginationControl",
                                "name": "max"})
            self.total_pages = int(max_pages[0].attrs['value'])
            return (True, None)

    def _threaded_scrape(self, bs4_parser: BeautifulSoup = None,
                         url: str = None) -> None:
        pass

    #Tuple[(bool), (Exception)]
    def task(self, id, url):
        print(f'starting scrape task: {id}')
        page = requests.get(url)
        # if page.status_code != 200:
        #     err = Exception("issue fetching page")
        #     return err
        # else:
        bs4_parser = BeautifulSoup(page.text, 'html.parser')
        results = bs4_parser.find_all("div", attrs={"class": "blog-list"})
        persist_post_list = []
        for post in results:
            title_and_link = post.find(
                "a", attrs={"class": "blog-list-title"})
            if title_and_link is not None:
                print(title_and_link)
                link = title_and_link.attrs['href']
                title = title_and_link.contents[0]
                if title is not None and link is not None:
                    persist_post = MidwestBlogModel(
                        link=(self.home_url + link), title=title)
                    persist_post_list.append(persist_post)
        repo = BlogRepoMidwest()
        repo.upsert_list(persist_post_list)

    def scrape(self):
        start_time = perf_counter()
        page = requests.get(self.blog_base_url)
        _initialization_result, err = self._initialize(
            bs4_parser=BeautifulSoup(page.text, 'html.parser'), page_obj=page)
        if err is not None:
            raise err
        for page_num in range(1, self.total_pages):
            str_url = self.blog_base_url + f'?page={page_num}'
            task = threading.Thread(target=self.task, args=(page_num, str_url))
            self.threads.append(task)
            task.start()
        for t in self.threads:
            t.join()
        end_time = perf_counter()
        print(
            f'It took {end_time- start_time: 0.2f} second(s) to complete scraping.')
