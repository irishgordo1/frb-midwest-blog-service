class MidwestBlogModel(object):
    def __init__(self, link=None, title=None) -> None:
        super().__init__()
        self.link = link
        self.title = title

    def __repr__(self) -> str:
        return repr(f'(Title: {self.title}, Link: {self.link})')
