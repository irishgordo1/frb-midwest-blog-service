from abc import ABC, abstractmethod

"""
BlogRepo: abstract persistance class, interface
"""


class BlogRepo(ABC):
    @abstractmethod
    def upsert(self, content):
        """performs persistance"""
        pass

    @abstractmethod
    def upsert_list(self, content_list):
        """list upsert"""
        pass
