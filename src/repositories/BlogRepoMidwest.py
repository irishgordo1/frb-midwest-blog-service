from src.repositories.BlogRepo import BlogRepo


class BlogRepoMidwest(BlogRepo):
    def __init__(self) -> None:
        super().__init__()
        pass

    def upsert(self, content):
        print(f'called persist with: {content}')
        pass

    def upsert_list(self, content_list):
        for post in content_list:
            print(post)
            self.upsert(post)
        pass
